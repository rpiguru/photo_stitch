import io
import os
import traceback
import time
from compoundpi.client import CompoundPiClient
from compoundpi.exc import CompoundPiNoServers, CompoundPiTransactionFailed
import datetime
import socket
import struct
import platform

if platform.system() == 'Linux':
    import fcntl


class CPiClient:

    target_dir = 'img'

    c = None

    def __init__(self, target_dir='img'):
        self.target_dir = target_dir
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)

        self.c = CompoundPiClient()

    def initialize(self):
        local_ip = get_ip_address('eth0')
        # Remove the final IP value
        net = '.'.join(local_ip.split('.')[:-1])

        self.c.servers.network = '{}.0/24'.format(net)
        self.c.servers.find(24)
        print 'Found {} servers'.format(len(self.c.servers))

    def take_pic(self):
        # Capture an image on all servers
        self.c.capture()

        # Download all available files from all servers
        try:
            for addr, files in self.c.list().items():
                for f in files:
                    if f.filetype == 'IMAGE':
                        print('Downloading image %d from %s (%d bytes)' % (f.index, addr, f.size,))
                        col_num = str(addr)[-2]
                        if not os.path.exists(self.target_dir + '/' + col_num):
                            os.makedirs(self.target_dir + '/' + col_num)
                        try:
                            with io.open('{}/{}/{}.jpg'.format(self.target_dir, col_num, str(addr)[-1]),
                                         'wb') as ff:
                                self.c.download(addr, f.index, ff)
                        except IOError as e:
                            print 'Error: ', e
            self.c.clear()
        except:
            print 'Failed to execute'
            traceback.print_exc()
        finally:
            # Wipe all files on all servers
            try:
                self.c.clear()
            except CompoundPiTransactionFailed as e:
                print e
            # print '-'*10, ' Finished running ', '-'*10
            # print 'Servers: ', self.c.servers
            self.c.close()


def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])


if __name__ == '__main__':

    cpi = CPiClient()
    cpi.initialize()

    cpi.take_pic()

    print 'Successfully executed...'


