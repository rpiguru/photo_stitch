import random

import cv2
import numpy as np


def stitch_image(img1='img/1.jpg', img2='img/2.jpg', result='img/result.jpg'):

    img_rgb1 = cv2.imread(img1)  # queryImage
    img_rgb2 = cv2.imread(img2)  # trainImage

    img1 = cv2.cvtColor(img_rgb1, cv2.COLOR_BGR2GRAY)
    img2 = cv2.cvtColor(img_rgb2, cv2.COLOR_BGR2GRAY)

    # Initiate SIFT detector
    orb = cv2.ORB()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)

    # create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    # Match descriptors.
    matches = bf.match(des1, des2)

    # Sort them in the order of their distance.
    matches = sorted(matches, key=lambda x: x.distance)

    # visualize the matches
    print '#matches:', len(matches)
    dist = [m.distance for m in matches]

    print 'distance: min: %.3f' % min(dist)
    print 'distance: mean: %.3f' % (sum(dist) / len(dist))
    print 'distance: max: %.3f' % max(dist)

    # threshold: half the mean
    thres_dist = (sum(dist) / len(dist)) * 0.5  # * 0.5
    print 'sum(dist): {} , len(dist):{} , thres_dist:{} '.format(sum(dist), len(dist), thres_dist)

    # keep only the reasonable matches
    sel_matches = [m for m in matches if m.distance < thres_dist]

    print '#selected matches:', len(sel_matches)

    # #####################################
    # visualization of the matches
    h1, w1 = img1.shape[:2]
    h2, w2 = img2.shape[:2]

    new_h1 = new_w1 = 0
    new_h2 = new_w2 = 0

    for i in range(len(sel_matches)):
        new_h1 += int(kp1[sel_matches[i].queryIdx].pt[1]) / len(sel_matches)
        new_w1 += int(kp1[sel_matches[i].queryIdx].pt[0]) / len(sel_matches)
        new_h2 += int(kp2[sel_matches[i].trainIdx].pt[1]) / len(sel_matches)
        new_w2 += int(kp2[sel_matches[i].trainIdx].pt[0]) / len(sel_matches)

    view = np.zeros(((new_h1 - new_h2 + h2), w1, 3), np.uint8)
    total_view = np.zeros(((h1 + h2), max(w1, w2), 3), np.uint8)

    view[:new_h1, :w1, :] = img_rgb1[:new_h1, :w1, :]
    if new_w1 < new_w2:
        view[new_h1:, :w2 - new_w2 + new_w1, :] = img_rgb2[new_h2:, new_w2 -new_w1:w2, :]
    else:
        view[new_h1:, new_w1 - new_w2:w1, :] = img_rgb2[new_h2:, :w1 - new_w1 + new_w2, :]

    total_view[:h1, :w1, :] = img_rgb1[:, :w1, :]
    total_view[h1:, :w2, :] = img_rgb2[:, :w2, :]

    for m in sel_matches:
        # draw the keypoints
        # print m.queryIdx, m.trainIdx, m.distance
        color = tuple([random.randint(0, 255) for _ in xrange(3)])
        cv2.line(total_view, (int(kp1[m.queryIdx].pt[0]), int(kp1[m.queryIdx].pt[1])), (int(kp2[m.trainIdx].pt[0]), int(kp2[m.trainIdx].pt[1] + h1)), color)

    cv2.imwrite(result, view)

    # cv2.imshow("view", view)
    # cv2.imshow("total lines", total_view)
    cv2.waitKey()


if __name__ == '__main__':

    stitch_image('img/4.jpg', 'img/5.jpg', 'img/45.jpg')

