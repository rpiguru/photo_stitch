import os
import platform

import time

from cpi_client import CPiClient
from stitch import Stitcher
import cv2
import imutils


default_width = 500
default_error_thresh = 30

if __name__ == '__main__':

    s_time = time.time()

    if platform.system() == 'Linux':
        cpi = CPiClient()
        cpi.initialize()
        cpi.take_pic()

    if not os.path.exists('result'):
        os.makedirs('result')

    for col in [name for name in os.listdir("img")]:
        imageA = cv2.imread("img/{}/1.jpg".format(col))
        imageB = cv2.imread("img/{}/2.jpg".format(col))
        imageA = imutils.resize(imageA, width=default_width)
        imageB = imutils.resize(imageB, width=default_width)

        stitcher = Stitcher()

        (result, vis, error) = stitcher.stitch([imageA, imageB], show_matches=True)
        # if platform.system() == 'Windows':
        #     cv2.imshow("1-2 keypoints", vis)
        #     cv2.imshow("1-2 results", result)

        imageB = cv2.imread("img/{}/3.jpg".format(col))
        imageB = imutils.resize(imageB, width=default_width)
        (result, vis, error) = stitcher.stitch([result, imageB], show_matches=True)
        # if platform.system() == 'Windows':
        #     cv2.imshow("1-2-3 keypoints", vis)
        #     cv2.imshow("1-2-3 results", result)

        imageB = cv2.imread("img/{}/4.jpg".format(col))
        imageB = imutils.resize(imageB, width=default_width)
        (result, vis, error) = stitcher.stitch([result, imageB], show_matches=True)
        cv2.imwrite('result/{}.jpg'.format(col), result)
        # if platform.system() == 'Windows':
        #     cv2.imshow("1-2-3-4 keypoints", vis)
        #     cv2.imshow("1-2-3-4 results", result)
        #     cv2.waitKey(0)

    print 'Elapsed: ', time.time() - s_time
